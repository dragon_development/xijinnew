<?php  return array (
  0 => 
  array (
    'aid' => '15',
    'catid' => '8',
    'typeid' => '1',
    'title' => '<font style="color:;">春天扮美家 绿色可是必不可少的利器</font>',
    'subtitle' => '',
    'tag' => '',
    'username' => 'cmseasy',
    'userid' => '127',
    'view' => '773',
    'color' => '',
    'strong' => '0',
    'toppost' => '0',
    'font' => '',
    'spid' => '1',
    'ip' => '',
    'mtitle' => '',
    'keyword' => '',
    'description' => '',
    'listorder' => '3101',
    'adddate' => '2011-11-27',
    'author' => 'cmseasy',
    'image' => '/',
    'thumb' => '/upload/images/201304/3101.jpg',
    'state' => '1',
    'checked' => '1',
    'introduce' => '提到春天，大家肯定首先会想到绿色，绿色的小草、绿色的树木，一切都焕发出勃勃生机。虽然时下天气仍热有些寒冷，但实际早已入春，大家不妨对家居做一些改造，为家中也增添一些绿色的家具或饰物，让家居也感受到春日的勃勃生机。
',
    'introduce_len' => '200',
    'content' => '<p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;">提到春天，大家肯定首先会想到绿色，绿色的小草、绿色的树木，一切都焕发出勃勃生机。虽然时下天气仍热有些寒冷，但实际早已入春，大家不妨对家居做一些改造，为家中也增添一些绿色的家具或饰物，让家居也感受到春日的勃勃生机。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><br/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><img src="/upload/images/201608/14703006621401.jpg" style="border: 0px none; vertical-align: middle; max-width: 100%; height: auto; display: block; margin: 10px auto;"/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><br/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;">客厅布置的小巧精致，墙面粉刷为简单的白色，搭配上轻盈的木色、灰色，给人的感觉轻松舒适。苹果绿是所有色彩中的点睛之笔，苹果绿的窗帘、抱枕点缀其中，营造出生机勃勃的感觉。洁白的砖块堆砌形状的沙发背墙，既属于客厅的一道装饰设计，又是区分客厅与餐厅空间机能的分界线。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><br/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><img src="/upload/images/201608/14703006632084.jpg" style="border: 0px none; vertical-align: middle; max-width: 100%; height: 477px; display: block; margin: 10px auto; width: 550px;"/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><br/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;">餐厅的椅子上同样也放了苹果绿的椅垫，和窗帘相互映衬，让人的心情瞬间明朗起来。依傍窗前就餐，白天有阳光为伴，晚上有夜景作陪，好不巧妙，与大自然的接触，仿佛触手可及。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><br/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><img src="/upload/images/201608/14703006648954.jpg" style="border: 0px none; vertical-align: middle; max-width: 100%; height: 550px; display: block; margin: 10px auto; width: 523px;"/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><br/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;">厨房为狭长型空间，墙体、橱柜和地板都是白色设计，从视觉上让空间更加开阔，同时给人明亮干净的空间质感。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><br/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><img src="/upload/images/201608/14703006651254.jpg" style="border: 0px none; vertical-align: middle; max-width: 100%; height: auto; display: block; margin: 10px auto;"/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><br/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;">卧室间的床头背景墙同样刷成了鲜艳的苹果绿，十分抢眼，一进房间就能看到，在搭配的黄色灯光和温润的木质家具，整个卧室空间弥漫出温馨气氛。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><br/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><img src="/upload/images/201608/14703006666178.jpg" style="border: 0px none; vertical-align: middle; max-width: 100%; height: auto; display: block; margin: 10px auto;"/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;"><br/></p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(51, 51, 51); line-height: 30px; text-indent: 32px; font-family: 微软雅黑; white-space: normal;">卧室连接一个南向阳台，大块的落地玻璃窗设计，满足了采用需求，在阳台处，一桌一椅便可轻松享受午后的休闲时光。</p><p><br/></p>',
    'template' => '0',
    'templatewap' => '0',
    'showform' => '0',
    'htmlrule' => '',
    'ishtml' => '0',
    'iswaphtml' => '0',
    'linkto' => '',
    'attr1' => '',
    'attr2' => '1650',
    'attr3' => '',
    'comment_num' => '0',
    'attachment_id' => '',
    'attachment_path' => '',
    'grade' => '0',
    'pics' => 'a:8:{i:1;a:2:{s:3:"url";s:37:"/upload/images/201304/duotu3101-1.jpg";s:3:"alt";s:0:"";}i:2;a:2:{s:3:"url";s:37:"/upload/images/201304/duotu3101-2.jpg";s:3:"alt";s:0:"";}i:3;a:2:{s:3:"url";s:37:"/upload/images/201304/duotu3101-3.jpg";s:3:"alt";s:0:"";}i:4;a:2:{s:3:"url";s:37:"/upload/images/201304/duotu3101-4.jpg";s:3:"alt";s:0:"";}i:5;a:2:{s:3:"url";s:37:"/upload/images/201304/duotu3101-5.jpg";s:3:"alt";s:0:"";}i:6;a:2:{s:3:"url";s:37:"/upload/images/201304/duotu3101-6.jpg";s:3:"alt";s:0:"";}i:7;a:2:{s:3:"url";s:37:"/upload/images/201304/duotu3101-7.jpg";s:3:"alt";s:0:"";}i:8;a:2:{s:3:"url";s:37:"/upload/images/201304/duotu3101-8.jpg";s:3:"alt";s:0:"";}}',
    'type' => '',
    'province_id' => '82',
    'city_id' => '116',
    'section_id' => '124',
    'outtime' => '0000-00-00',
    'my_size' => '',
    'my_zhaopinbumen' => '',
    'my_jobtype' => '',
    'my_jobtitle' => '',
    'my_jobnumber' => '',
    'my_jobgender' => '',
    'my_jobwork' => '',
    'my_jobacademic' => '',
    'my_jobage' => '',
    'my_jobworkareas' => '',
    'my_jobrequirements' => '',
    'my_contactname' => '',
    'isecoding' => '0',
    'ecoding' => '',
    'my_tonghangbijiao' => '',
    'my_donggongjindu' => '',
    'my_zhiwei' => '',
    'iscomment' => '1',
    'url' => '/index.php?case=archive&act=show&aid=15',
    'catname' => '家居装修',
    'caturl' => '/index.php?case=archive&act=list&catid=8',
    'stitle' => '春天扮美家 绿色可是必不可少的利器',
    'strgrade' => '<img src="/images/admin/star2.gif" border="0" /><img src="/images/admin/star2.gif" border="0" /><img src="/images/admin/star2.gif" border="0" /><img src="/images/admin/star2.gif" border="0" /><img src="/images/admin/star2.gif" border="0" />',
    'buyurl' => '/index.php?case=archive&act=orders&aid=15',
    'oldprice' => '1650',
    'intro' => '',
    'sthumb' => '//upload/images/201304/3101.jpg',
  ),
  1 => 
  array (
    'aid' => '18',
    'catid' => '8',
    'typeid' => '2',
    'title' => '<font style="color:;">体验现代家居生活</font>',
    'subtitle' => '',
    'tag' => '',
    'username' => 'cmseasy',
    'userid' => '127',
    'view' => '40',
    'color' => '',
    'strong' => '0',
    'toppost' => '0',
    'font' => '',
    'spid' => '1',
    'ip' => '',
    'mtitle' => '',
    'keyword' => '',
    'description' => '',
    'listorder' => '3102',
    'adddate' => '2011-11-27',
    'author' => 'cmseasy',
    'image' => '/',
    'thumb' => '/upload/images/201304/3102.jpg',
    'state' => '1',
    'checked' => '1',
    'introduce' => '',
    'introduce_len' => '200',
    'content' => '<p>　　当清晨的阳光洒向你的窗户，这是一天的开始。家是一天的起点，也是终点。</p><p>当你回到家，迎接你的是家人最纯真的笑脸，它让你忘却一切烦恼。掌上明珠家居现代生活方式正要为您提供便捷、舒适的生活方式，让家人时时陪伴在您身边。</p><p>　　追求华丽、高雅的古典风格。居室色彩主调为白色。家具为古典弯腿式，家具、门、窗漆成白色。擅用各种花饰、丰富的木线变化、富丽的窗帘帷幄是西式传统室内装饰的固定模式，空间环境多表现出华美、富丽、浪漫的气氛。</p><p><br/></p><p style="text-align: center;"><img src="/upload/images/201608/14710969889257.jpg" title="14710969889257.jpg" alt="94387_hui.jpg"/></p><p><br/></p><p>　　现代美式风格特点</p><p><br/></p><p>　　1、客厅简洁明快</p><p><br/></p><p>　　客厅作为待客区域，一般要求简洁明快，同时装修较其它空间要更明快光鲜，通常使用大量的石材和木饰面装饰;美国人喜欢有历史感的东西，这不仅反映在软装摆件上对仿古艺术品的喜爱，同时也反映在装修上对各种仿古墙地砖、石材的偏爱和对各种仿旧工艺的追求上。总体而言，美式田园风格的客厅是宽敞而富有历史气息的。</p><p><br/></p><p>　　2、厨房开敞</p><p><br/></p><p>　　厨房在美国人眼中一般是开敞的(由于其饮食烹饪习惯)，同时需要有一个便餐台在厨房的一隅，还要具备功能强大又简单耐用的厨具设备，如水槽下的残渣粉碎机，烤箱等。</p><p><br/></p><p>　　需要有容纳双开门冰箱的宽敞位置和足够的操作台面。在装饰上也有很多讲究，如喜好仿古面的墙砖、橱具门板喜好用实木门扇或是白色模压门扇仿木纹色。另外，厨房的窗也喜欢配置窗帘等。</p><p><br/></p><p>　　3、卧室布置温馨</p><p><br/></p><p>　　美式家居的卧室布置较为温馨，作为主人的私密空间，主要以功能性和实用舒适为考虑的重点，一般的卧室不设顶灯，多用温馨柔软的成套布艺来装点，同时在软装和用色上非常统一。</p><p><br/></p><p>　　4、书房简单实用</p><p><br/></p><p>　　美式家居的书房简单实用，但软装颇为丰富，各种象征主人过去生活经历的陈设一应俱全，被翻卷边的古旧书籍、颜色发黄的航海地图、乡村风景的油画、一支鹅毛笔……即使是装饰品，这些东西也足以为书房的美式风格加分。家庭室：家庭室作为家庭成员休息交流的中心，属私密性很强的空间，一般设于餐厅旁，并有电视机，同时沙发和座椅选择轻松明快的式样，室内绿化也较为丰富，装饰画较多。</p><p><br/></p>',
    'template' => 'archive/show_pic.html',
    'templatewap' => '0',
    'showform' => '0',
    'htmlrule' => '',
    'ishtml' => '0',
    'iswaphtml' => '0',
    'linkto' => '',
    'attr1' => '',
    'attr2' => '500',
    'attr3' => '',
    'comment_num' => '0',
    'attachment_id' => '',
    'attachment_path' => '',
    'grade' => '0',
    'pics' => 'N;',
    'type' => '',
    'province_id' => '82',
    'city_id' => '116',
    'section_id' => '124',
    'outtime' => '0000-00-00',
    'my_size' => '',
    'my_zhaopinbumen' => '',
    'my_jobtype' => '',
    'my_jobtitle' => '',
    'my_jobnumber' => '',
    'my_jobgender' => '',
    'my_jobwork' => '',
    'my_jobacademic' => '',
    'my_jobage' => '',
    'my_jobworkareas' => '',
    'my_jobrequirements' => '',
    'my_contactname' => '',
    'isecoding' => '0',
    'ecoding' => '',
    'my_tonghangbijiao' => '',
    'my_donggongjindu' => '',
    'my_zhiwei' => '',
    'iscomment' => '1',
    'url' => '/index.php?case=archive&act=show&aid=18',
    'catname' => '家居装修',
    'caturl' => '/index.php?case=archive&act=list&catid=8',
    'stitle' => '体验现代家居生活',
    'strgrade' => '<img src="/images/admin/star2.gif" border="0" /><img src="/images/admin/star2.gif" border="0" /><img src="/images/admin/star2.gif" border="0" /><img src="/images/admin/star2.gif" border="0" /><img src="/images/admin/star2.gif" border="0" />',
    'buyurl' => '/index.php?case=archive&act=orders&aid=18',
    'oldprice' => '500',
    'intro' => '',
    'sthumb' => '//upload/images/201304/3102.jpg',
  ),
  2 => 
  array (
    'aid' => '10',
    'catid' => '8',
    'typeid' => '2',
    'title' => '<font style="color:;">美式风格四居室家居装修设计效果图</font>',
    'subtitle' => '',
    'tag' => '',
    'username' => 'cmseasy',
    'userid' => '127',
    'view' => '3',
    'color' => '',
    'strong' => '0',
    'toppost' => '0',
    'font' => '',
    'spid' => '0',
    'ip' => '',
    'mtitle' => '',
    'keyword' => '',
    'description' => '',
    'listorder' => '3103',
    'adddate' => '2011-11-27',
    'author' => 'cmseasy',
    'image' => '/',
    'thumb' => '/upload/images/201304/3103.jpg',
    'state' => '1',
    'checked' => '1',
    'introduce' => '',
    'introduce_len' => '200',
    'content' => '<p style="text-align: center;"><img src="/upload/images/201608/14710970922048.jpg" title="14710970922048.jpg" alt="21705552_1463630005218_690x460.jpg"/></p>',
    'template' => '0',
    'templatewap' => '0',
    'showform' => '0',
    'htmlrule' => '',
    'ishtml' => '0',
    'iswaphtml' => '0',
    'linkto' => '',
    'attr1' => '',
    'attr2' => '360',
    'attr3' => '',
    'comment_num' => '0',
    'attachment_id' => '',
    'attachment_path' => '',
    'grade' => '0',
    'pics' => 'N;',
    'type' => '',
    'province_id' => '82',
    'city_id' => '116',
    'section_id' => '124',
    'outtime' => '0000-00-00',
    'my_size' => '',
    'my_zhaopinbumen' => '',
    'my_jobtype' => '',
    'my_jobtitle' => '',
    'my_jobnumber' => '',
    'my_jobgender' => '',
    'my_jobwork' => '',
    'my_jobacademic' => '',
    'my_jobage' => '',
    'my_jobworkareas' => '',
    'my_jobrequirements' => '',
    'my_contactname' => '',
    'isecoding' => '0',
    'ecoding' => '',
    'my_tonghangbijiao' => '',
    'my_donggongjindu' => '',
    'my_zhiwei' => '',
    'iscomment' => '1',
    'url' => '/index.php?case=archive&act=show&aid=10',
    'catname' => '家居装修',
    'caturl' => '/index.php?case=archive&act=list&catid=8',
    'stitle' => '美式风格四居室家居装修设计效果图',
    'strgrade' => '<img src="/images/admin/star2.gif" border="0" /><img src="/images/admin/star2.gif" border="0" /><img src="/images/admin/star2.gif" border="0" /><img src="/images/admin/star2.gif" border="0" /><img src="/images/admin/star2.gif" border="0" />',
    'buyurl' => '/index.php?case=archive&act=orders&aid=10',
    'oldprice' => '360',
    'intro' => '',
    'sthumb' => '//upload/images/201304/3103.jpg',
  ),
);