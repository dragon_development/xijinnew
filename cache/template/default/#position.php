<?php defined('ROOT') or exit('Can\'t Access !'); ?>
<nav class="sub_menu">
<div class="container">
<?php
if(front::get('case') == 'area'){
?>
<h3><?php echo area::getpositonhtml(get('province_id'),get('city_id'),get('section_id'),get('id'));?></h3>
<?php
}elseif($topid==0){
?>
<?php
}elseif(front::get('case') == 'announ'){
?>
<h3><a title="<?php echo lang(siteannoun);?>" href="#"><?php echo lang(siteannoun);?></a></h3>
<?php
}elseif(front::get('case') == 'guestbook'){
?>
<h3><a title="<?php echo lang('guestbook');?>" href="#"><?php echo lang('guestbook');?></a></h3>
<?php
}elseif(front::get('case') == 'comment'){
?>
<h3><a title="<?php echo $t['name'];?>" href="<?php echo $t['url'];?>"><?php echo lang('commentlist');?></a></h3>
<?php
}elseif(front::get('case') == 'type'){
?>
<h3><?php echo type::getpositionhtml($type['typeid']);?></h3>
<?php
}elseif(front::get('case') == 'special'){
?>
<h3><a title="<?php echo $special['title'];?>" href="#"><?php echo $special['title'];?></a></h3>
<?php
}elseif(front::get('case') == 'tag'){
?>
<h3><a title="<?php echo $tag;?>" href="#"><?php echo $tag;?></a></h3>
<?php
}elseif(front::get('case') == 'mailsubscription'){
?>
<h3><a href="#" title="<?php echo lang(mailsubscription);?>"><?php echo lang(mailsubscription);?></a></h3>
<?php
}else{
?>
<h3><?php echo $category[$catid]['catname'];?></h3>
<?php foreach(categories($catid) as $i => $t) { ?>
<a href="<?php echo $t['url'];?>"<?php if($t['catid']==$catid) { ?> class="sub_menu_on"<?php } elseif ($i<1) { ?> class="sub_menu_on"<?php } ?>><?php echo $t['catname'];?></a>
<?php } ?>
<?php
}
?>

</div>

</nav>

<!--/sub_menu-->