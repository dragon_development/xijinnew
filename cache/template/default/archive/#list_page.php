<?php defined('ROOT') or exit('Can\'t Access !'); ?>
<?php echo template('header.html'); ?>
<!-- 面包屑导航开始 -->
<?php echo template('position.html'); ?>
<!-- 面包屑导航结束 -->

<!-- 页面标题开始 -->
<div class="title">
<div class="container">
<h3><?php echo $category[$catid]['catname'];?></h3>
<p><?php echo $category[$catid]['htmldir'];?></p>
</div>
</div>
<!-- 页面标题结束 -->


<!-- 中部开始 -->
<div class="container">

<p class="t_tools" style="float:right;padding:0px 15px;background:#eee;text-align:right;font-size:14px;">
<a href="javascript:CallPrint('print');">打印本文</a> &nbsp; &nbsp; &nbsp; <a href="javascript:doZoom(14)">小</a>&nbsp; &nbsp;<a href="javascript:doZoom(18)">中</a>&nbsp; &nbsp;<a href="javascript:doZoom(20)">大</a>
</p>
<script src="<?php echo $skin_path;?>/js/c_tool.js" type="text/javascript" ></script><!--用于打印和文字放大-->




<div class="blank30"></div>
<div class="content" id="print">
<!-- 内容 -->

<div id="content_text">
<!-- 内容 -->
<?php
$page = intval(front::$get['page']);
if($page==0)$page=1;
$content = $category[$catid][categorycontent];
$contents = preg_split('%<div style="page-break-after(.*?)</div>%si', $content);
if ($contents) {
$pages = count($contents);
front::$record_count = $pages * config::get('list_pagesize');
$category[$catid][categorycontent] = $contents[$page - 1];
}
?>
<?php echo $category[$catid]['categorycontent'];?>

<div class="blank30"></div>
<?php if($pages>1) { ?>
<!-- 内页分页 -->
<div class="blank10"></div>
<div class="pages">
<?php echo category_pagination($catid);?>
</div>
<div class="blank30"></div>
<?php } ?>
</div>
<!-- 内容结束 -->
</div>

<!-- 分享开始 -->
<?php if(get('share')=='1') { ?>
<div class="blank30"></div>
<?php echo template('system/share.html'); ?>
<?php } ?>
<!-- 分享结束 -->




<div class="clear"></div>

</div>
</div>


<!-- 页底推荐图文产品开始 -->
<?php echo templatetag::tag('内容页底图文产品三条');?>
<!-- 页底推荐图文产品结束 -->

<!-- 限定图片宽度 -->
<?php echo template('width_max.html'); ?>
<!-- 限定图片宽度 -->
<?php echo template('footer.html'); ?>