<?php defined('ROOT') or exit('Can\'t Access !'); ?>
<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="CmsEasy 5_6_0_20160825_UTF8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta content="telephone=no" name="format-detection">
<meta http-equiv="x-rim-auto-match" content="none">
<title><?php echo lang(sitemap);?></title>
<meta name="keywords" content="<?php echo getKeywords($archive,$category,$catid,$type);?>" />
<meta name="description" content="<?php echo getDescription($archive,$category,$catid,$type);?>" />
<meta name="author" content="CmsEasy Team" />
<link rel="icon" href="<?php echo $base_url;?>/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="<?php echo $skin_path;?>/css/sitemap.css" />

</head>
<body>


<div class="box">
<div class="logo"><a title="<?php echo get(sitename);?>" href="<?php echo $base_url;?>/"><img src="<?php echo get('site_logo');?>" alt="<?php echo get(sitename);?>" width="<?php echo get(logo_width);?>" /></a></div>
</div>

<div class="t_1">
<div>
<h3><?php echo lang(sitemap);?></h3>
<p>Site Map</p>
</div>
</div>


<div class="box">
<div class="blank30"></div>

<!-- 正文 -->
<?php if(is_array($archive) && !empty($archive))
foreach($archive as $row) { ?>
<a href="<?php echo $row['url'];?>" target="_blank"><?php echo $row['catname'];?></a><br />
<?php } ?>
<div class="blank20"></div>

<?php if(hasflash()) { ?>
<div style="color:red;font-size:16px;"><?php echo showflash(); ?></div><?php } ?>

<div class="blank30"></div>
</div>
<div class="blank30"></div>
<div class="blank30"></div>
<div class="copy">
<?php echo get(site_right);?> <a title="<?php echo get('sitename');?>" href="<?php echo $base_url;?>/"><?php echo get('sitename');?></a> All Rights Reserved. 
<div class="clear"></div>
Powered by <a href="http://www.cmseasy.cn" title="CmsEasy企业网站系统" target="_blank">CmsEasy</a>
</div>
</body>
</html>