<?php defined('ROOT') or exit('Can\'t Access !'); ?>
<?php echo template('header.html'); ?>

<?php echo template('system/slide.html'); ?>



<!-- about section -->
<section class="about" id="about">
<div class="container">
<div class="row about-w3ls1">
<?php echo templatetag::tag('首页第一行栏目图片说明');?>
</div>
</div>
</section>
<!-- /about section -->
<!-- info section -->
<section class="info">
<div class="container">
<div class="row">
<div class="col-lg-4 col-md-4 info-w3ls1">
<h3><?php echo templatetag::tag('首页第二行栏目');?></h3>
</div>
<div class="col-lg-8 col-md-8 info-w3ls2">
<div class="col-sm-6 info-agile">
<i class="fa fa-briefcase" aria-hidden="true"></i>
<?php echo templatetag::tag('首页第二行子栏目一');?>
</div>
<div class="col-sm-6 info-agile">
<i class="fa fa-camera-retro" aria-hidden="true"></i>
<?php echo templatetag::tag('首页第二行子栏目二');?>
</div>
<div class="col-sm-6 info-agile">
<i class="fa fa-cubes" aria-hidden="true"></i>
<?php echo templatetag::tag('首页第二行子栏目三');?>
</div>
<div class="col-sm-6 info-agile">
<i class="fa fa-diamond" aria-hidden="true"></i>
<?php echo templatetag::tag('首页第二行子栏目四');?>
</div>				
</div>
</div>
</div>	
</section>
<!-- /info section -->
<!-- divider section -->
<section class="divider">
<ul class="rslides" id="slider1">
<?php echo templatetag::tag('首页第三行栏目图文幻灯');?>
</ul>		
</section>
<!-- /divider section -->
<!-- work section -->
<section class="work" id="work">
<div class="container">
<?php echo templatetag::tag('首页第四行栏目');?>
<?php echo templatetag::tag('首页第四行栏目图片8条');?>
</div>
</section>
<!-- /work section -->
<!-- services section -->
<section class="service" id="service">
<div class="container">
<div class="row">
<div class="col-lg-4 col-md-4 serv-agile1">
<h3><?php echo templatetag::tag('首页第五行栏目');?></h3>
</div>
<div class="col-lg-8 col-md-8 serv-agile2">
<div class="col-sm-6">
<div class="col-xs-5 serv-details1">
<div class="hi-icon-wrap hi-icon-effect-5 hi-icon-effect-5a">
<span class="glyphicon glyphicon-home"></span>
</div>	
</div>
<div class="col-xs-7 serv-details2">
<div class="serv-w3ls">
<?php echo templatetag::tag('首页第五行栏目一');?>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="col-xs-5 serv-details1">
<div class="hi-icon-wrap hi-icon-effect-5 hi-icon-effect-5a">
<span class="glyphicon glyphicon-king"></span>
</div>	
</div>
<div class="col-xs-7 serv-details2">
<div class="serv-w3ls">
<?php echo templatetag::tag('首页第五行栏目二');?>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="col-xs-5 serv-details1">
<div class="hi-icon-wrap hi-icon-effect-5 hi-icon-effect-5a">
<span class="glyphicon glyphicon-blackboard"></span>
</div>	
</div>
<div class="col-xs-7 serv-details2">
<div class="serv-w3ls">
<?php echo templatetag::tag('首页第五行栏目三');?>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="col-xs-5 serv-details1">
<div class="hi-icon-wrap hi-icon-effect-5 hi-icon-effect-5a">
<span class="glyphicon glyphicon-lamp"></span>
</div>	
</div>
<div class="col-xs-7 serv-details2">
<div class="serv-w3ls">
<?php echo templatetag::tag('首页第五行栏目四');?>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- /services section -->
<!-- team section -->
<section class="team" id="team">
<div class="container">
<?php echo templatetag::tag('首页第六行栏目');?>
<div class="row">
<?php echo templatetag::tag('首页第六行栏目图片3条');?>
</div>
</div>
</section>
<!-- /team section -->
<!-- subscribe section -->
<section class="subs">
<div class="container">
<h3 class="text-center"><?php echo lang(search);?></h3>
<div class="row">
<div class="col-lg-12">
<div class="subscribe">
<form name='search' action="<?php echo url('archive/search');?>" onsubmit="search_check();" method="post">
<div class="form-group1">
<input class="form-control" type="text" id="input10" name="keyword" placeholder="<?php echo lang(pleaceinputtext);?>"  required>
</div>
<div class="form-group2">
<button class="btn btn-outline btn-lg" name='submit' type="submit"><?php echo lang(search);?></button>
</div>
<div class="clear-fix"></div>
</form>
</div>	
</div>
</div>	
</div>
</section>
<!-- /subscribe section -->
<script src="<?php echo $skin_path;?>/js/responsiveslides.min.js"></script>
<script>
    // You can also use "$(window).load(function() {"
    $(function () {

      // Slideshow 1
      $("#slider1").responsiveSlides({
        maxwidth: 1920,
        speed: 800
      });

  // Slideshow 2
      $("#slider2").responsiveSlides({
        maxwidth: 1920,
        speed: 800
      });
      
  // Slideshow for banner
      $("#slider").responsiveSlides({
        maxwidth: 1920,
        speed: 800
      });
  
    });
</script>



<?php echo template('footer.html'); ?>