<?php return array (
 2 => 
  array (
    'id' => 2,
    'name' => '首页第一行栏目图片说明',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '1',
      'tagtemplate' => 'tag_category_lanmutupian.html',
      'submit' => '提交',
      'attr1' => '0',
      'catname' => '',
      'htmldir' => '',
      'introduce' => '',
    ),
  ),
  3 => 
  array (
    'id' => 3,
    'name' => '首页第二行栏目',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '66',
      'tagtemplate' => 'tag_category.html',
      'submit' => '提交',
      'attr1' => '0',
      'catname' => '',
      'htmldir' => '',
      'introduce' => '',
    ),
  ),
  4 => 
  array (
    'id' => '4',
    'name' => '首页第二行子栏目一',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '67',
      'tagtemplate' => 'tag_category_lanmushuoming.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  5 => 
  array (
    'id' => '5',
    'name' => '首页第二行子栏目二',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '68',
      'tagtemplate' => 'tag_category_lanmushuoming.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  6 => 
  array (
    'id' => '6',
    'name' => '首页第二行子栏目三',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '69',
      'tagtemplate' => 'tag_category_lanmushuoming.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  7 => 
  array (
    'id' => '7',
    'name' => '首页第二行子栏目四',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '70',
      'tagtemplate' => 'tag_category_lanmushuoming.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  8 => 
  array (
    'id' => '8',
    'name' => '首页第三行栏目图文幻灯',
    'tagfrom' => 'content',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '2',
      'son' => '1',
      'typeid' => '0',
      'spid' => '0',
      'province_id' => '0',
      'city_id' => '0',
      'section_id' => '0',
      'length' => '20',
      'introduce_length' => '100',
      'ordertype' => 'adddate-desc',
      'istop' => '0',
      'limit' => '8',
      'tagtemplate' => 'tag_content_pic1.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  9 => 
  array (
    'id' => '9',
    'name' => '首页第四行栏目',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '3',
      'tagtemplate' => 'tag_category_lanmushuoming2.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  10 => 
  array (
    'id' => 10,
    'name' => '首页第四行栏目图片8条',
    'tagfrom' => 'content',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '3',
      'son' => '1',
      'typeid' => '0',
      'spid' => '0',
      'province_id' => '0',
      'city_id' => '0',
      'section_id' => '0',
      'length' => '8',
      'introduce_length' => '30',
      'ordertype' => 'adddate-desc',
      'istop' => '0',
      'limit' => '8',
      'tagtemplate' => 'tag_content_pic2.html',
      'submit' => '提交',
      'attr1' => '0',
      'catname' => '',
      'htmldir' => '',
      'introduce' => '',
    ),
  ),
  11 => 
  array (
    'id' => 11,
    'name' => '首页第五行栏目',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '76',
      'tagtemplate' => 'tag_category.html',
      'submit' => '提交',
      'attr1' => '0',
      'catname' => '',
      'htmldir' => '',
      'introduce' => '',
    ),
  ),
  12 => 
  array (
    'id' => 12,
    'name' => '首页第五行栏目一',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '77',
      'tagtemplate' => 'tag_category_lanmushuoming.html',
      'submit' => '提交',
      'attr1' => '0',
      'catname' => '',
      'htmldir' => '',
      'introduce' => '',
    ),
  ),
  13 => 
  array (
    'id' => 13,
    'name' => '首页第五行栏目二',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '78',
      'tagtemplate' => 'tag_category_lanmushuoming.html',
      'submit' => '提交',
      'attr1' => '0',
      'catname' => '',
      'htmldir' => '',
      'introduce' => '',
    ),
  ),
  1 => 
  array (
    'id' => 1,
    'name' => '首页第五行栏目三',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '80',
      'tagtemplate' => 'tag_category_lanmushuoming.html',
      'submit' => '提交',
      'attr1' => '0',
      'catname' => '',
      'htmldir' => '',
      'introduce' => '',
    ),
  ),
  25 => 
  array (
    'id' => 25,
    'name' => '首页第五行栏目四',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '79',
      'tagtemplate' => 'tag_category_lanmushuoming.html',
      'submit' => '提交',
      'attr1' => '0',
      'catname' => '',
      'htmldir' => '',
      'introduce' => '',
    ),
  ),
  26 => 
  array (
    'id' => 26,
    'name' => '首页第六行栏目',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '62',
      'tagtemplate' => 'tag_category_lanmushuoming2.html',
      'submit' => '提交',
      'attr1' => '0',
      'catname' => '',
      'htmldir' => '',
      'introduce' => '',
    ),
  ),
  27 => 
  array (
    'id' => '27',
    'name' => '首页第六行栏目图片3条',
    'tagfrom' => 'content',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '62',
      'son' => '1',
      'typeid' => '0',
      'spid' => '0',
      'province_id' => '0',
      'city_id' => '0',
      'section_id' => '0',
      'length' => '6',
      'introduce_length' => '30',
      'ordertype' => 'adddate-desc',
      'istop' => '0',
      'limit' => '3',
      'tagtemplate' => 'tag_content_pic3.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  14 => 
  array (
    'id' => '14',
    'name' => '页底栏目一',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '1',
      'tagtemplate' => 'tag_category-foot-cat-list.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  15 => 
  array (
    'id' => '15',
    'name' => '页底栏目二',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '3',
      'tagtemplate' => 'tag_category-foot-cat-list.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  16 => 
  array (
    'id' => '16',
    'name' => '页底栏目三',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '2',
      'tagtemplate' => 'tag_category-foot-cat-list.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  17 => 
  array (
    'id' => '17',
    'name' => '内容页底图文产品三条',
    'tagfrom' => 'content',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '3',
      'son' => '1',
      'typeid' => '0',
      'spid' => '0',
      'province_id' => '0',
      'city_id' => '0',
      'section_id' => '0',
      'length' => '20',
      'introduce_length' => '',
      'ordertype' => 'adddate-desc',
      'istop' => '0',
      'limit' => '3',
      'thumb' => 'on',
      'attr1' => '0',
      'tagtemplate' => 'tag_content-content-bottom.html',
      'submit' => '提交',
    ),
  ),
  18 => 
  array (
    'id' => '18',
    'name' => '内页右侧栏目名称',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '2',
      'tagtemplate' => 'tag_category-index-title.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  19 => 
  array (
    'id' => '19',
    'name' => '内页右侧内容列表',
    'tagfrom' => 'content',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '2',
      'son' => '1',
      'typeid' => '0',
      'spid' => '0',
      'province_id' => '0',
      'city_id' => '0',
      'section_id' => '0',
      'length' => '20',
      'introduce_length' => '',
      'ordertype' => 'adddate-desc',
      'istop' => '0',
      'limit' => '10',
      'attr1' => '0',
      'tagtemplate' => 'tag_content.html',
      'submit' => '提交',
    ),
  ),
  20 => 
  array (
    'id' => '20',
    'name' => '首页第四行滚动图片',
    'tagfrom' => 'content',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '10',
      'son' => '1',
      'typeid' => '0',
      'spid' => '0',
      'province_id' => '0',
      'city_id' => '0',
      'section_id' => '0',
      'length' => '12',
      'introduce_length' => '',
      'ordertype' => 'adddate-desc',
      'istop' => '0',
      'limit' => '10',
      'attr1' => '0',
      'tagtemplate' => 'tag_content-gundong.html',
      'submit' => '提交',
    ),
  ),
  21 => 
  array (
    'id' => '21',
    'name' => '首页第六行栏目名称',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '14',
      'tagtemplate' => 'tag_category-title.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  22 => 
  array (
    'id' => '22',
    'name' => '首页第一行栏目名称',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '1',
      'tagtemplate' => 'tag_category-title-index-1.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  23 => 
  array (
    'id' => '23',
    'name' => '首页第一行栏目图片说明四',
    'tagfrom' => 'category',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '7',
      'tagtemplate' => 'tag_category-index-1.html',
      'submit' => '提交',
      'attr1' => '0',
    ),
  ),
  24 => 
  array (
    'id' => 24,
    'name' => '首页第六行图片12张',
    'tagfrom' => 'content',
    'tagcontent' => 'null',
    'setting' => 
    array (
      'onlymodify' => '',
      'catid' => '14',
      'son' => '1',
      'typeid' => '0',
      'spid' => '0',
      'province_id' => '0',
      'city_id' => '0',
      'section_id' => '0',
      'length' => '',
      'introduce_length' => '',
      'ordertype' => 'adddate-desc',
      'istop' => '0',
      'limit' => '12',
      'thumb' => 'on',
      'attr1' => '0',
      'tagtemplate' => 'tag_content-index-6.html',
      'submit' => '提交',
      'catname' => '',
      'htmldir' => '',
      'introduce' => '',
    ),
  ),
);